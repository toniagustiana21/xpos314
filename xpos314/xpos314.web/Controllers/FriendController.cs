﻿using Microsoft.AspNetCore.Mvc;
using xpos314.web.Models;

namespace xpos314.web.Controllers
{
    public class FriendController : Controller
    {
        private static List<Friend> list = new List<Friend>()
        {
            new Friend(){ Id = 1, Name = "Anwar", Address = "Ragunan" },
            new Friend(){ Id = 2, Name = "Asti", Address = "Garut"},
        };
        public IActionResult Index()
        {


            

            ViewBag.listFriend = list;

            

            return View(list);
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Friend friend)
        {
            list.Add(friend);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Friend friend = list.Find(a => a.Id == id)!;
            return View(friend);
        }

        [HttpPost]
        public IActionResult Edit(Friend data)
        {
            Friend friend = list.Find(a => a.Id == data.Id)!;
            int index = list.IndexOf(friend);

            if(index > -1)
            {
                list[index].Id = data.Id;
                list[index].Name = data.Name;
                list[index].Address = data.Address;
            }
            

            return RedirectToAction("Index");
        }
        public IActionResult Detail(int id)
        {
            Friend friend = list.Find(a => a.Id == id)!;
            return View(friend);
        }

        //public IActionResult Delete(int id)
        //{
        //    Friend friend = list.Find(a => a.Id == id)!;
        //    return View(friend);
        //}

        //[HttpPost]
        //public IActionResult Delete(string id)
        //{
        //    Friend data = list.Find(a => a.Id == int.Parse(id))!;
        //    list.Remove(data);
        //    return RedirectToAction("Index");
        //}

        [HttpGet]
        [HttpPost]
        public IActionResult Delete(int id)
        {
            Friend friend = list.Find(a => a.Id == id)!;
            if(HttpContext.Request.Method == "POST")
            {
                list.Remove(friend);
                return RedirectToAction("Index");
            }
            else
            {
                return View(friend);
            }
        }
    }
}
