﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CategoryTryController : Controller
    {
        private readonly XPOS_314Context db;
        private readonly CategoryTryServices categoryTryServices;

        public CategoryTryController(XPOS_314Context _db)
        {
            this.db = _db;
            this.categoryTryServices = new CategoryTryServices(db);
        }

        public IActionResult Index()
        {
            List<VMTblCategory> dataView = categoryTryServices.GetAllData();
            return View(dataView);
        }

        public IActionResult Create()
        {
            VMTblCategory dataView = new VMTblCategory();
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Create(VMTblCategory dataView)
        {
            VMResponse respon = new VMResponse();

            if (ModelState.IsValid)
            {
                respon = categoryTryServices.Create(dataView);

                if (respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Edit(int id)
        {
            VMTblCategory dataView = categoryTryServices.GetById(id);
            return View(dataView);
        }
        [HttpPost]
        public IActionResult Edit(VMTblCategory dataView)
        {
            VMResponse respon = new VMResponse();

            if (ModelState.IsValid)
            {
                respon = categoryTryServices.Edit(dataView);

                if (respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            respon.Entity = dataView;
            return View(respon.Entity);

        }
        public IActionResult Detail(int id)
        {
            VMTblCategory dataView = categoryTryServices.GetById(id);
            return View(dataView);
        }
        public IActionResult Delete(int id)
        {
            VMTblCategory dataView = categoryTryServices.GetById(id);
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Delete(VMTblCategory dataView)
        {
            VMResponse respon = categoryTryServices.Delete(dataView);
            if (respon.Success)
            {
                return RedirectToAction("Index");
            }
            return View(respon.Entity);
        }
    }
}
