﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CustomerController : Controller
    {
        private CustomerServices customerServices;
        private RoleServices roleServices;
        private int IdUser = 1;


        public CustomerController(RoleServices _roleServices, CustomerServices _customerServices)
        {
            this.roleServices = _roleServices;
            this.customerServices = _customerServices;

            //this.configuration = _configuration;
            //this.categoryServices = new CategoryServices(this.configuration);
            //this.variantServices = new VariantServices(this.configuration);
        }
        public async Task<IActionResult> Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber,
                                               int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder)  ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;

            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMTblCustomer> data = await customerServices.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameCustomer.ToLower().Contains(searchString.ToLower())
                || a.RoleName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameCustomer).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameCustomer).ToList();
                    break;
            }
            return View(PaginatedList<VMTblCustomer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Create()
        {
            VMTblCustomer data = new VMTblCustomer();

            List<TblRole> listRole = await roleServices.GetAllData();
            ViewBag.ListRole = listRole;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMTblCustomer dataParam)
        {

            VMResponse respon = await customerServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });

            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMTblCustomer data = await customerServices.GetDataById(id);

            List<TblRole> listRole = await roleServices.GetAllData();
            ViewBag.listRole = listRole;

            return PartialView(data);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMTblCustomer dataParam)
        {
            VMResponse respon = await customerServices.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMTblCustomer data = await customerServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {

            VMResponse respon = await customerServices.Delete(id);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Detail(int id)
        {
            VMTblCustomer data = await customerServices.GetDataById(id);
            return PartialView(data);
        }
    }
}
