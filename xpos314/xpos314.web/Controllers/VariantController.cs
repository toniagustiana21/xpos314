﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class VariantController : Controller
    {
        private CategoryServices categoryServices;
        private VariantServices variantServices;
        private int IdUser = 1;


        public VariantController(CategoryServices _categoryServices, VariantServices _variantServices)
        {
            this.categoryServices = _categoryServices;
            this.variantServices = _variantServices;

            //this.configuration = _configuration;
            //this.categoryServices = new CategoryServices(this.configuration);
            //this.variantServices = new VariantServices(this.configuration);
        }

        public async Task<IActionResult> Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber,
                                               int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;

            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMTblVariant> data = await variantServices.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameVariant.ToLower().Contains(searchString.ToLower())
                || a.NameCategory.ToLower().Contains(searchString.ToLower())
                || a.Description.ToLower().Contains(searchString.ToLower()) 
                
                ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameVariant).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameVariant).ToList();
                    break;
            }

            return View(PaginatedList<VMTblVariant>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Create()
        {
            VMTblVariant data = new VMTblVariant();

            List<TblCategory> listCategory = await categoryServices.GetAllData();
            ViewBag.ListCategory = listCategory;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMTblVariant dataParam)
        {
           
            VMResponse respon = await variantServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });

            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMTblVariant data = await variantServices.GetDataById(id);

            List<TblCategory> listCategory = await categoryServices.GetAllData();
            ViewBag.ListCategory = listCategory;

            return PartialView(data);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMTblVariant dataParam)
        {
            VMResponse respon = await variantServices.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            VMTblVariant data = await variantServices.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMTblVariant data = await variantServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {

            VMResponse respon = await variantServices.Delete(id);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }
    }
}
