﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class RoleController : Controller
    {
        private RoleServices roleServices;
        private int IdUser = 1;

        public RoleController(RoleServices _roleServices)
        {
            this.roleServices = _roleServices;
        }
        public async Task<IActionResult> Index(string sortOrder,
                                               string searchString,
                                               string currentFilter,
                                               int? pageNumber,
                                               int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;

            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<TblRole> data = await roleServices.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.RoleName.ToLower().Contains(searchString.ToLower())
                ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
            }
            return View(PaginatedList<TblRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            TblRole data = new TblRole();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(TblRole dataParam)
        {
            dataParam.CreateBy = IdUser;
            VMResponse respon = await roleServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });

            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string roleName)
        {
            bool isExist = await roleServices.CheckRoleByName(roleName);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblRole data = await roleServices.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblRole dataParam)
        {
            dataParam.UpdateBy = IdUser;
            VMResponse respon = await roleServices.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            TblRole data = await roleServices.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblRole data = await roleServices.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> SureDelete(int id)
        {
            int createBy = IdUser;
            VMResponse respon = await roleServices.Delete(id, createBy);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }
    }
}
