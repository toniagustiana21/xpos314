﻿using Microsoft.AspNetCore.Mvc;
using xpos314.web.Services;
using xpos314.viewmodels;
using Newtonsoft.Json;

namespace xpos314.web.Controllers
{
    public class OrderController : Controller
    {
        private ProductServices productServices;
        private OrderServices orderServices;
        private int IdUser = 1;

        public OrderController(ProductServices _productServices, OrderServices _orderServices)
        {
            this.productServices = _productServices;
            this.orderServices = _orderServices;
        }
        public async Task<IActionResult> Catalog(VMSearchPage dataSearch)
        {
            List<VMTblProduct> dataProduct = await productServices.GetAllData();
            dataSearch.MinAmount = dataSearch.MinAmount == null ? decimal.MinValue : dataSearch.MinAmount;
            dataSearch.MaxAmount = dataSearch.MaxAmount == null ? decimal.MaxValue : dataSearch.MaxAmount;

            if(dataSearch.NameProduct != null)
            {
                dataProduct = dataProduct.Where(a => a.NameProduct == dataSearch.NameProduct).ToList();
            }
            if(dataSearch.MinAmount != null && dataSearch.MaxAmount != null)
            {
                dataProduct = dataProduct.Where(a => a.Price >= dataSearch.MinAmount &&
                              a.Price <= dataSearch.MaxAmount).ToList();
            }

            VMOrderHeader dataHeader = HttpContext.Session.GetComplexData<VMOrderHeader>("ListCart");
            if(dataHeader == null)
            {
                dataHeader = new VMOrderHeader();
                dataHeader.ListDetails = new List<VMOrderDetail>();
            }
            var ListDetail = JsonConvert.SerializeObject(dataHeader.ListDetails);
            ViewBag.dataHeader = dataHeader;
            ViewBag.dataDetail = ListDetail;
            //end set session

            ViewBag.Search = dataSearch;
            ViewBag.CurrentPageSize = dataSearch.CurrentPageSize;

            

            return View(PaginatedList<VMTblProduct>.CreateAsync(dataProduct, dataSearch.pageNumber ?? 1,
                dataSearch.pageSize ?? 3));
        }

        //set session in VMOrderHeader every add or minus
        [HttpPost]
        public JsonResult SetSession(VMOrderHeader dataHeader)
        {
            HttpContext.Session.SetComplexData("ListCart", dataHeader);

            return Json("");
        }
        //end

        //remove session
        public JsonResult RemoveSession()
        {
            HttpContext.Session.Remove("ListCart");

            return Json("");
        }

        public IActionResult OrderPreview(VMOrderHeader dataHeader)
        {
            return PartialView(dataHeader);
        }
        public IActionResult SearchMenu()
        {
            return PartialView();
        }

        [HttpPost]
        public async Task<JsonResult> SubmitOrder(VMOrderHeader dataHeader)
        {
            //int IdCustomer = HttpContext.Session.GetInt32("IdCustomer") ?? 0;
            //dataHeader.IdCostumer = IdCustomer;

            dataHeader.IdCostumer = IdUser;

            VMResponse respon = await orderServices.SubmitOrder(dataHeader);
            return Json(respon);
        }

        public async Task<IActionResult> HistoryOrder(VMSearchPage dataSearch)
        {
            //int IdCustomer = HttpContext.Session.GetInt32("IdCustomer") ?? 0;
            int IdCustomer = IdUser;
            List<VMOrderHeader> data = await orderServices.ListHeaderDetails(IdCustomer);
            int count = await orderServices.CountTransaction(IdCustomer);
            ViewBag.Count = count;

            dataSearch.MinDate = dataSearch.MinDate == null ? DateTime.MinValue : dataSearch.MinDate;
            dataSearch.MaxDate = dataSearch.MaxDate == null ? DateTime.MaxValue : dataSearch.MaxDate;

            dataSearch.MinAmount = dataSearch.MinAmount == null ? decimal.MinValue : dataSearch.MinAmount;
            dataSearch.MaxAmount = dataSearch.MaxAmount == null ? decimal.MaxValue : dataSearch.MaxAmount;

            if (!string.IsNullOrEmpty(dataSearch.CodeTransaction))
            {
                data = data.Where(a => a.CodeTransaction.ToLower()
                                        .Contains(dataSearch.CodeTransaction.ToLower())).ToList();
            }

            if(dataSearch.MinDate != null && dataSearch.MaxDate != null)
            {
                data = data.Where(a => a.CreateDate >= dataSearch.MinDate && 
                                        a.CreateDate <= dataSearch.MaxDate).ToList();

            }

            if(dataSearch.MinAmount != null && dataSearch.MaxAmount != null)
            {
                data = data.Where(a => a.Amount >= dataSearch.MinAmount &&
                                        a.Amount <= dataSearch.MaxAmount).ToList();

            }

            ViewBag.search = dataSearch;

            return View(data);
        }

        public IActionResult Search()
        {
            return PartialView();
        }
    }
}
