﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class OrderServices
    {
        public static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public OrderServices(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }

        public async Task<VMResponse> SubmitOrder(VMOrderHeader dataHeader)
        {
            var json = JsonConvert.SerializeObject(dataHeader);
            var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiOrder/SubmitOrder", content);

            var apiRespon = await request.Content.ReadAsStringAsync();
            VMResponse respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            return respon;
        }

        public async Task<List<VMOrderHeader>> ListHeaderDetails(int IdCustomer)
        {
            string apiRespon = await client.GetStringAsync
                               (RouteAPI + $"apiOrder/GetDataOrderHeaderDetail/{IdCustomer}");
            List<VMOrderHeader> listHeader = JsonConvert.DeserializeObject<List<VMOrderHeader>>(apiRespon)!;

            return listHeader;
        }

        public async Task<int> CountTransaction(int IdCustomer)
        {
            string apiRespon = await client.GetStringAsync
                               (RouteAPI + $"apiOrder/CountTrasaction/{IdCustomer}");
            int count = JsonConvert.DeserializeObject<int>(apiRespon);

            return count;
        }
    }
}
