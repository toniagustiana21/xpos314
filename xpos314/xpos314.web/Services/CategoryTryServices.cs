﻿using AutoMapper;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class CategoryTryServices
    {
        private readonly XPOS_314Context db;
        VMResponse respon = new VMResponse();
        int idUser = 1;

        public CategoryTryServices(XPOS_314Context _db)
        {
            this.db = _db;
        }

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblCategory, VMTblCategory>();
                cfg.CreateMap<VMTblCategory, TblCategory>();
            });

            IMapper mapper = config.CreateMapper();
            return mapper;
        }
        public List<VMTblCategory> GetAllData()
        {
            List<TblCategory> dataModel = db.TblCategories.Where(a => a.IsDelete == false).ToList();

            List<VMTblCategory> dataView = GetMapper().Map<List<VMTblCategory>>(dataModel);

            return dataView;
        }

        public VMResponse Create(VMTblCategory dataView)
        {
            TblCategory dataModel = new TblCategory();
            dataModel = GetMapper().Map<TblCategory>(dataView);
            dataModel.CreateBy = idUser;
            dataModel.CreateDate = DateTime.Now;
            dataModel.IsDelete = false;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                respon.Message = "Data success saved";
                respon.Entity = dataModel;
            }
            catch(Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + e.Message;
                respon.Entity = dataView;
            }

            return respon;
        }

        public VMTblCategory GetById(int id)
        {
            TblCategory dataModel = db.TblCategories.Find(id)!;
            VMTblCategory dataView = GetMapper().Map<VMTblCategory>(dataModel);
            return dataView;
        }

        public VMResponse Edit(VMTblCategory dataView)
        {
            TblCategory dataModel = db.TblCategories.Find(dataView.Id)!;
            dataModel.NameCategory = dataView.NameCategory;
            dataModel.Description = dataView.Description;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data Success Saved";
                respon.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved" + e.Message;
                respon.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            return respon;
        }
        
        public VMResponse Delete(VMTblCategory dataView)
        {
            TblCategory dataModel = db.TblCategories.Find(dataView.Id)!;
            dataModel.IsDelete = true;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data Success Saved";
                respon.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            catch(Exception e)
            {
                respon.Success=false;
                respon.Message = "Failed Saved : " + e.Message;
                respon.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            return respon;
        }
    }
}
