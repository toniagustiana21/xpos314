﻿using AutoMapper;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class VariantTryServices
    {
        private readonly XPOS_314Context db;
        VMResponse respon = new VMResponse();
        int idUser = 1;

        public VariantTryServices(XPOS_314Context _db)
        {
            this.db = _db;
        }

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblVariant, VMTblVariant>();
                cfg.CreateMap<VMTblVariant, TblVariant>();
            });

            IMapper mapper = config.CreateMapper();
            return mapper;
        }
        public List<VMTblVariant> GetAllData()
        {
            List<VMTblVariant> dataView = (from v in db.TblVariants
                                           join c in db.TblCategories on v.IdCategory equals c.Id
                                           where v.IsDelete == false
                                           select new VMTblVariant
                                           {
                                               Id = v.Id,
                                               NameVariant = v.NameVariant,
                                               Description = v.Description,

                                               IdCategory = v.IdCategory,
                                               NameCategory = c.NameCategory
                                           }).ToList();
            return dataView;
        }
        public VMResponse Create(VMTblVariant dataView)
        {
            TblVariant dataModel = new TblVariant();
            dataModel = GetMapper().Map<TblVariant>(dataView);
            //dataModel.IdCategory = dataView.IdCategory;
            dataModel.CreateBy = idUser;
            dataModel.CreateDate = DateTime.Now;
            dataModel.IsDelete = false;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                respon.Message = "Data success saved";
                respon.Entity = dataModel;
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + e.Message;
                respon.Entity = dataView;
            }

            return respon;
        }

        public VMTblVariant GetById(int id)
        {
            VMTblVariant dataView = (from v in db.TblVariants
                                     join c in db.TblCategories on v.IdCategory equals c.Id
                                     where v.IsDelete == false && v.Id == id
                                     select new VMTblVariant
                                     {
                                         Id = v.Id,
                                         NameVariant = v.NameVariant,
                                         Description = v.Description,
                                         CreateBy = v.CreateBy,
                                         CreateDate = v.CreateDate,
                                         UpdateBy = v.UpdateBy,
                                         UpdateDate = v.UpdateDate,
                                         IdCategory = v.IdCategory,
                                         NameCategory = c.NameCategory
                                     }).FirstOrDefault()!;
            return dataView;
        }
        public VMResponse Edit(VMTblVariant dataView)
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id)!;

            dataModel.NameVariant = dataView.NameVariant;
            dataModel.IdCategory = dataView.IdCategory;
            dataModel.Description = dataView.Description;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success saved";
                respon.Entity = dataModel;
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + e.Message;
                respon.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }
            return respon;
        }
        public VMResponse Delete(VMTblVariant dataView)
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id)!;
            dataModel.IsDelete = true;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data Success Saved";
                respon.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + e.Message;
                respon.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }
            return respon;
        }
    }
}
